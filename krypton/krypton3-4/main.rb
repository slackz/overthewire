load 'lib/files.rb'
load 'lib/counts.rb'

# From Norvig's analysis of google books library
ENG_RANKED = 'ETAOINSRHLDCUMFPGWYBVKXJQZ'
TXT_RANKED = 'SQJUBNGCDZVWMYTXKELAFIORHP'

# Iterated through various diff substring counts and common digrams / trigrams
#   reading through decrypted text and substituting reasonable values
CRYPT_MAP  = {
  'J' => 't', 'D' => 'h', 'S' => 'e',
  'Q' => 'a', 'G' => 'n', 'W' => 'd',
  'E' => 'g', 'H' => 'q', 'Z' => 'c',
  'V' => 'l', 'N' => 'r', 'U' => 's',
  'X' => 'f', 'C' => 'i', 'L' => 'y',
  'F' => 'k', 'K' => 'w', 'T' => 'm',
  'O' => 'x', 'I' => 'v', 'R' => 'j',
  'B' => 'o', 'M' => 'u', 'Y' => 'p',
  'A' => 'b', 'P' => 'z'
}

def decrypt(str)
  d = ->(char) { CRYPT_MAP[char] || char }
  str.split('').map { |c| d.(c) }.join
end

f, c, c2 = Files.new, Counts.new(1), Counts.new(3)
f.files.each { |_, str| [c, c2].each { |obj| obj.substr_count(str) } }

puts '-------------------------------------------'
puts CRYPT_MAP.inspect
puts CRYPT_MAP.count
puts c.counts.inspect
puts c2.counts.select{|k,v| v > 5}.inspect
puts c2.counts.select{|k,v| v > 5}.keys.map {|s| decrypt(s)}.inspect

f.files.each do |name, str|
  puts '-----------------------------'
  puts name.upcase + "\n"
  puts decrypt(str)
end

puts '------------------------------'
puts 'ANSWER!!!'
puts decrypt(File.read('krypton4'))
