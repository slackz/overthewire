class Counts

  def initialize(num_chars)
    @num_chars = num_chars
    @counts = {}
  end

  def counts
    @counts.sort_by { |k, v| v }.reverse.to_h
  end

  def substr_count(str)
    substrs = str.scan(/(?=(.{#{@num_chars}}))/).flatten
    substrs.each { |elem| @counts[elem] ||= 0; @counts[elem] += 1 }
  end
end
